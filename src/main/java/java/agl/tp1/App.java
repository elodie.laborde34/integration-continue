package java.agl.tp1;

import java.lang.management.OperatingSystemMXBean;

public class App 

{
    public static void main( String[] args )
    {
    	GestionTER gestion = new GestionTER();
    	gestion.addGroupe("A");
    	gestion.addGroupe("B");
    	gestion.addGroupe("C");
    	gestion.addSujet("Les additions");
    	gestion.addSujet("Les multiplications");
    	gestion.addSujet("Les divisions");
    	gestion.addSujet("Les soustractions");
    	gestion.affecterSujets();
    	gestion.generateHTMLGroupesSujets();
    	
    }
}
