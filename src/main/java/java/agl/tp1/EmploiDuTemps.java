package java.agl.tp1;
import java.util.*;


public class EmploiDuTemps {
	public ArrayList<Groupe> groupes;
	
	//pour chaque prof, on va parcourir le tableau du groupe pour affecter tous les creneaux du tableau du prof !
	public void affectationCreneau(ArrayList<Groupe> groupes) {
		ArrayList<Prof> listeProf= new ArrayList<Prof>();
		for(int i=0 ; i<groupes.size() ; i++) { //on parcoure la liste des groupes
			if (!(listeProf.contains(groupes.get(i).getEncadrant()))) { //on attrape un prof, uniquement si il n'a pas été attrapé
				Prof prof1=groupes.get(i).getEncadrant();//alors on le chope pour lui faire son emploi du temps
				int nCreneau=0;
				for(int j=0 ; j<groupes.size() ; j++) {
					if(groupes.get(j).getEncadrant()==prof1) {
						groupes.get(i).setCreneauxPassage(prof1.getCreneaux().get(nCreneau));//on accorde le creneau au groupe, et puis on incrémente nCreneau
						nCreneau=nCreneau++;
					}
				}
				listeProf.add(prof1);
			}
		}
		//à la fin, n'ont pas de creneau ceux qui n'ont pas de prof
	}
	
	public void creationPlanning() {
		Object[][] planning= new Object[3][];
		for(int i=0 ; i<groupes.size() ; i++) {
			planning[0][i]=groupes.get(i).getNom();
			planning[1][i]=groupes.get(i).getEncadrant().getNom();
			planning[2][i]=groupes.get(i).getCreneauxPassage();
		}
	}
}
