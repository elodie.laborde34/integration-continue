Créer un projet sous gitlab, avec le code de l'application de votre choix :
j'ai choisi pour ce projet de reprendre le code d'un projet de l'année dernière, une application java en charge de distribuer des projets à des groupes, celle ci s'appelle Gestion TER

Implémenter l'ensemble des triggers (rules dans l'éditor gitlab) qui sont pour vous nécessaire à l'execution d'une CI :
Dans la réflexion de ce qui aurait pu être mise en place dans un contexte professionel, nous avons créé des rules pour le build où on l'on décide de build quand on push et quand on merge. Quand le build a succès pour le merge, utiliser les dependances d'un build succes pour exécuter les tests (toujours avec des rules).

Avec l'utilisation d'un/plusieurs runner seft hosted :
nous n'en avons utilisé qu'un mais ce dernier a su être suffisant pour le projet

Votre pipeline devra executer des linters :
nous avons utilisé sonarqube après de multiples essais pour faire fonctionner sonarcloud en vain

Des tests unitaires :
ceux ci dont dans le dossier src/test/java/

De la qualité de code : résultat sur le screen de sonarqube.


En conclusion, la mise en place d'un projet sous GitLab avec la mise en place d'une intégration continue nous a permis de nous rendre compte qu'il s'agit d'un processus essentiel pour tout projet de développement logiciel. Dans le cadre de notre projet de reprise de l'application Gestion TER, nous avons pu mettre en place un environnement de développement collaboratif et automatisé, permettant une gestion efficace du code source et une amélioration de la qualité de code. La mise en place de règles et de dépendances pour les builds et les tests a permis une automatisation complète de la chaîne de développement, augmentant ainsi la productivité et la qualité de l'application. La mise en place d'un pipeline CI/CD est donc un élément clé de tout projet de développement logiciel moderne et nous n'hésiterons pas à le réutiliser dans des projets futurs pour améliorer la qualité de code de nos applications.
